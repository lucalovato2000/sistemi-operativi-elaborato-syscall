Tutto ciò non espressamente specificato nel testo del progetto è a scelta dello studente. 

È vietato l’uso di funzioni C per la gestione del file system (e.g. fopen). Il progetto deve funzionare su sistema operativo Ubuntu 18 e Repl, rispettare il template fornito, ed essere compilabile con il comando make.

L’ammissione all’esame orale è possibile solo se rispettata la data di consegna dell’elaborato nel secondo semestre. 

Si consiglia di svolgere il progetto a gruppi di tre persone ma si possono creare gruppi anche più piccoli se necessario. Ogni gruppo dovrà consegnare un solo progetto

    La prima consegna dell’elaborato (primo semestre) consente di ottenere un punteggio massimo di tre trentesimi (3/30).
    La seconda consegna dell’elaborato permette un punteggio massimo di ventiquattro24 trentesimi (24/30).
    La consegna dell’esercitazione su MentOS/process-management, e del presente elaborato permette un punteggio massimo di ventisette trentesimi (27/30).
    La consegna dell’esercitazione su MentOS/process-management, MentOS/memory-management, e del presente elaborato permette un punteggio massimo di ventinove trentesimi (29/30).

Il punteggio della prima consegna viene sommato ai punteggi della seconda consegna. La votazione massima è quindi 29+3=32 corrispondente a 30 e lode.

Consegna elaborato e-learning

Si richiede di rispettare la seguente struttura di cartelle per la consegna dell’elaborato:

---------------------------------------------------------------------------------
/sistemi_operativi
            /system_call/ (tutti gli file del template che vi andremmo a fornire)
            /MentOS/scheduler_algorithm.c (da consegnare nel secondo semestre)
            /MentOS/deadlock.c (da consegnare nel secondo semestre)
---------------------------------------------------------------------------------

La directory sistemi_operativi deve essere compressa in un archivio di nome <matricola1_matricola2_matricola3>_sistemi_operativi.tar.gz. 

Attenzione: matricola è VR123456 e non id123rvx. 

L’archivio deve essere creato con il comando tar (no programmi esterni )
Esempio: tar -czvf VR123456_VR654321_VR321123_sistemi_operativi.tar.gz. sistemi_operativi/ 

In caso di gruppi con meno di 3 studenti si mettano nel nome del archivio (tar.gz) solo le matricole degli studenti che hanno partecipato. L’archivio tar.gz deve essere caricato nell’apposita sezione sul sito di e-learning che renderemo disponibile a breve. 

Nota bene: gli elaborati consegnati dovranno passare un test preliminare di compilazione ed esecuzione automatica. Gli elaborati che non passano tale test perché non rispettano le specifiche non saranno considerati validi per l’ammissione all’orale, quindi si chiede di seguire fedelmente le indicazioni ed il formato del template.
Esempio: Anche chi, invece di chiamare una directory system_call la chiama system-call prende automaticamente  0 (zero).
Scadenze:

    Seconda consegna elaborato: deadline 06/06/2021 ore 23.59

    Prenotazione slot nel calendario esami-orali: deadline 31/05/2021 ore 23.59

    Date esami orali: 14/06/2021-25/06/2021 (Lunedi - Venerdì)

FAQ progetto d'esame:

    Posso usare printf, scanf e le funzioni di string.h?
    E' possibile usare printf, scanf e tutte le funzioni della libreria string.h
    Posso usare sprintf?
    E' possibile usare sprintf
    Posso definire globalmente le chiavi per msg, shm, e semafori?
    E' possibile definire globalmente tutto quello che lo studente richiede necessario (anche le chiavi IPC)
    Posso aggiungere altri file sorgente al progetto?
    E' possibile aggiungere altri file sorgente al progetto. (Ripartire le funzionalità di un programma in più funzioni e più file sorgente è sempre una buona pratica di sviluppo!) 
    Quali argomenti possono essere chiesti nell'esame scritto di laboratorio?
    L'esame scritto riguarderà tutti gli argomenti trattati in classe (vedi slide)
    Cosa viene chiesto nell'esame scritto?
    Strutturalmente l'esame comprenderà 2/3 esercizi su system calls e MentOS. I contenuti degli esercizi svolti in laboratorio ed il progetto d'esame possono essere presi come esempio per l'esame scritto.
    
    
Eseguire il progetto tramite il comando da terminale: 
./sender_manager InputFiles/F0.csv & ./receiver_manager & ./hackler InputFiles/F7.csv
