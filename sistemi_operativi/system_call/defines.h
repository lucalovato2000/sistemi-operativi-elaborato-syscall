/// @file defines.h
/// @brief Contiene la definizioni di variabili
///        e funzioni specifiche del progetto.

#pragma once

#define SIZE 900
#define FAILS == -1
#define FAKE_SIGNAL 1234
#define TOTAL_PROCESSES 6

// Colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef struct {
    int id;
    char message[50];
    char id_sender;
    char id_receiver;
    char timeArrival[150];
    char timeDeparture[150];
    int hoursArrival;
    int minutesArrival;
    int secondsArrival;
    int hoursDeparture;
    int minutesDeparture;
    int secondsDeparture;
    int delayS1;
    int delayS2;
    int delayS3;
} sender_traffic_struct; // traffic struct

typedef struct {
    int semNum;
    int semVal;
} sem_value; // sem_value struct

typedef struct {
    long mtype;
    int id;
    char message[50];
    char id_sender;
    char id_receiver;
    char timeArrival[150];
    char timeDeparture[150];
    int hoursArrival;
    int minutesArrival;
    int secondsArrival;
    int hoursDeparture;
    int minutesDeparture;
    int secondsDeparture;
    int delayS1;
    int delayS2;
    int delayS3;
} sender_traffic_queue_struct; // traffic struct queue

typedef struct{
    int id;
    char message[50];
    char id_sender;
    char id_receiver;
    int delayS1;
    int delayS2;
    int delayS3;
    char type[10];
} sender_struct; // instructions struct

typedef struct {
    char *IPC;
    int IDKey;
    char *creator;
    char creationTime[10];
    char distructionTime[10];
} ipc_struct; // ipc struct

typedef struct {
    int pid1;
    int pid2;
    int pid3;
} pid_list; // pid struct

void writeFile(int, int, int, char *);
void cleanArray(char *, int);
void errorOpenWithoutCreate(int);
void errorOpen(int);
void headerOnFile(int, int, int);
int totalCharInFile(int ,char *,int , int);
const char *getfield(char* ,int);
int numberOfRows(int, int);
void removeChar(char*, char);
void printSenderTraffic(sender_traffic_struct*, int);
char itoa(int);
char itoaKey(key_t);
void printStruct(int, sender_traffic_struct*, int);
void printStructQueue(int, sender_traffic_queue_struct *, int);
void printIPCStruct(int, ipc_struct *, int);
struct tm *add_time(struct tm*, int);
void getFormattedTimewithDelay(sender_traffic_struct *, int, int);   
void getFormattedTimewithDelayMessageQueue(sender_traffic_queue_struct *, int, int);
void clearSenderTraffic(sender_traffic_struct*, int);
void clearSenderTrafficQueue(sender_traffic_queue_struct *, int); 
void printSenderInstruction(sender_struct*, int);
void printIPC(ipc_struct *, int);
int countDigit(int);
void updateIPC(ipc_struct *, int , char *, int, char *, char *, char *);
void timef(char *);
void timeDeparture(time_t, struct tm *, char *);
void timeTermination(time_t, struct tm *, char *);
pid_list getPIDFromFile(int, int, int);
void searchAndReplaceOnFile(int, int, char *, char *);
int numberOfFieldInFile(int, int);
void copyToSenderTrafficSH(sender_traffic_struct *, sender_traffic_struct *, int, int, int);