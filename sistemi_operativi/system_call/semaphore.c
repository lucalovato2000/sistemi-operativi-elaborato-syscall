/// @file semaphore.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione dei semaphores.

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include "defines.h"
#include "err_exit.h"
#include "semaphore.h"

// Create a semaphore set
semSet semCreateSet(key_t sem_key, int size) {
    semSet set = semget(sem_key, size, IPC_CREAT | S_IRWXU);
    if (set FAILS) {
        ErrExit("Unable to create semaphore set");
    }
    return set;
}

// Initialize a semaphore set
void semInit(semSet set, semVal *array) {
    semun data;
    data.array = array;
    if (semctl(set, 0, SETALL, data) FAILS) {
        ErrExit("Unable to initialize semaphore set");
    }
}

// Make operation using semaphore
void semOperation(int semid, unsigned short sem_num, short sem_op) {
    struct sembuf sop = {.sem_num = sem_num, .sem_op = sem_op, .sem_flg = 0};

    if (semop(semid, &sop, 1) == -1){
        ErrExit("semop failed");
    }
}

// Print the semaphore set's state
void printSemaphoresValue(int semId) {
    unsigned short semVal[6];
    semun arg;
    arg.array = semVal;

    // get the current state of the set
    if (semctl(semId, 0, GETALL, arg) == -1) {
        ErrExit("semctl GETALL failed");
    }

    // print the semaphore's value
    printf("Semaphore set state:\n");

    for (int i = 0; i < 6; i++) {
        printf("semVal: %d\n", semVal[i]);
    }
}

// Print the semaphore set's state
void print3SemaphoresValue(int semId) {
    unsigned short semVal[3];
    semun arg;
    arg.array = semVal;

    // get the current state of the set
    if (semctl(semId, 0, GETALL, arg) == -1) {
        ErrExit("semctl GETALL failed");
    }

    // print the semaphore's value
    printf("Semaphore set state:\n");

    for (int i = 0; i < 3; i++) {
        printf("semVal: %d\n", semVal[i]);
    }
}