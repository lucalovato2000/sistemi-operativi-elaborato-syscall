/// @file msg_queue.c
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione delle message queue.

#pragma once
#include <sys/msg.h>
#include "defines.h"

int messageCreate(key_t);
void printQueueOnTerminal(sender_traffic_queue_struct *, int);
void printQueueOnFile(int, sender_traffic_queue_struct *, int );
void copyToSenderTrafficQueue(sender_traffic_queue_struct *, sender_traffic_struct *, int );
void copyToSenderTrafficQueue2(sender_traffic_queue_struct *, sender_traffic_struct *, int, int, int);
void removeQ(int);