/// @file pipe.c
/// @brief Contiene l'implementazione delle funzioni
///        specifiche per la gestione delle PIPE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "defines.h"
#include "err_exit.h"
#include "pipe.h"

// Create a PIPE
void createPipe(int *pipeFD) {
    if (pipe(pipeFD) FAILS) {
        ErrExit("PIPE failed\n");
    }
}

// Close read-end of PIPE
void closeReadEndPipe(int *pipeFD){
    if (close(pipeFD[0]) FAILS) {
        ErrExit("PIPE close read-end failed");
    }
}

// Close write-end of PIPE
void closeWriteEndPipe(int *pipeFD) {
    if (close(pipeFD[1]) FAILS) {
        pErrExit("PIPE close write-end failed %d", pipeFD);
    }
}