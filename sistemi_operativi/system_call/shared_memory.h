/// @file shared_memory.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione della shared_memory.

#pragma once
#include <sys/shm.h>
#include "defines.h"

int shmCreate(key_t, int);
void *shmAttach(int, int);
void shmDetach(void *);
void removeSH(int);
void printOnTerminalSH(sender_traffic_struct *, int);